package com.test.service;

import com.test.entity.Blog;

import java.util.List;

/**
 * @author wuwei
 * @date 2018/10/29 11:06
 */
public interface BlogService {
    String demo();

    String add(Blog blog);

    List<Blog> findAll();

    List<Blog> findByKeyWord(String title, String summary, String content, int pageIndex, int pageSize);
}
