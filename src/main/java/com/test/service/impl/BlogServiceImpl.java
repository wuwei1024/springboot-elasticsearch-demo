package com.test.service.impl;

import com.test.dao.BlogRepository;
import com.test.entity.Blog;
import com.test.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * @author wuwei
 * @date 2018/10/29 11:06
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public String demo() {
        //清除所有数据
        blogRepository.deleteAll();
        // 存入数据
        blogRepository.save(new Blog("登鹳雀楼", "王之涣的诗", "白日依山尽，黄河入海流。欲穷千里目，更上一层楼。"));
        blogRepository.save(new Blog("相思", "王维的诗", "红豆生南国，春来发几枝。愿君多采撷，此物最相思。"));
        blogRepository.save(new Blog("静夜思", "李白的诗", "床前明月光，疑是地上霜。举头望明月，低头思故乡。"));
        return "SUCCESS";
    }

    @Override
    public String add(Blog blog) {
        blogRepository.save(blog);
        return "SUCCESS";
    }

    @Override
    public List<Blog> findAll() {
        Iterable<Blog> all = blogRepository.findAll();
        List<Blog> blogList = new LinkedList<>();
        all.forEach(blogList::add);
        return blogList;
    }

    @Override
    public List<Blog> findByKeyWord(String title, String summary, String content, int pageIndex, int pageSize) {
        Pageable pageable = new PageRequest(pageIndex, pageSize);
        Page<Blog> page = blogRepository.findDistinctEsBlogByTitleContainingOrSummaryContainingOrContentContaining(title, summary, content, pageable);
        return page.getContent();
    }
}