package com.test.controller;

import com.test.entity.Blog;
import com.test.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wuwei
 * @date 2018/10/25 13:42
 */
@RestController
@RequestMapping("/blog")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @RequestMapping("/demo")
    public String demo() {
        return blogService.demo();
    }

    @RequestMapping("/add")
    public String add(Blog blog) {
        return blogService.add(blog);
    }

    @RequestMapping("/findAll")
    public List<Blog> findAll() {
        return blogService.findAll();
    }

    @RequestMapping("/findByKeyWord")
    public List<Blog> findByKeyWord(@RequestParam("title") String title,
                                    @RequestParam("summary") String summary,
                                    @RequestParam("content") String content,
                                    @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                                    @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        return blogService.findByKeyWord(title, summary, content, pageIndex, pageSize);
    }
}