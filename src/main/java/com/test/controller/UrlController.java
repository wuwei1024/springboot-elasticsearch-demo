package com.test.controller;

import com.test.util.LocalHostIP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * @author wuwei
 * @date 2018/10/29 11:03
 */
@RestController
public class UrlController {

    @Autowired
    private Environment env;

    @RequestMapping("/getUrl")
    public String getUrl() throws UnknownHostException {
        String host = InetAddress.getLocalHost().getHostAddress();
        String port = this.env.getProperty("server.port");
        return String.format("http://%s:%s", host, port);
    }

    @RequestMapping("/getAllUrl")
    public Map getAllUrl() throws SocketException {
        return LocalHostIP.getAllLocalHostIP();
    }
}