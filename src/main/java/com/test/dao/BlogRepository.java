package com.test.dao;

import com.test.entity.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author wuwei
 * @date 2018/10/25 13:27
 */
public interface BlogRepository extends ElasticsearchRepository<Blog, String> {

    Page<Blog> findDistinctEsBlogByTitleContainingOrSummaryContainingOrContentContaining(String title, String summary, String content, Pageable pageable);

}
