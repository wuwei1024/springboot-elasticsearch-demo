package com.test.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author wuwei
 * @date 2018/10/25 13:23
 */
@Document(indexName = "test", type = "blog")
@Data
public class Blog {

    @Id
    private String id;
    private String title;
    private String summary;
    private String content;

    public Blog(String title, String summary, String content) {
        this.title = title;
        this.summary = summary;
        this.content = content;
    }
}
