package com.test;

import com.test.dao.BlogRepository;
import com.test.entity.Blog;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author wuwei
 * @date 2018/10/29 10:20
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BlogRepositoryTest {
    @Autowired
    private BlogRepository blogRepository;

    @Before
    public void initRepository() {
        //清除所有数据
        blogRepository.deleteAll();
        //存入数据
        blogRepository.save(new Blog("登鹳雀楼", "王之涣的诗", "白日依山尽，黄河入海流。欲穷千里目，更上一层楼。"));
        blogRepository.save(new Blog("相思", "王维的诗", "红豆生南国，春来发几枝。愿君多采撷，此物最相思。"));
        blogRepository.save(new Blog("静夜思", "李白的诗", "床前明月光，疑是地上霜。举头望明月，低头思故乡。"));
    }

    @Test
    public void test() {
        Pageable pageable = new PageRequest(0, 20);
        String title = "思";
        String summary = "相思";
        String content = "相思";
        Page<Blog> page = blogRepository.findDistinctEsBlogByTitleContainingOrSummaryContainingOrContentContaining(title, summary, content, pageable);
        System.out.println(page.getTotalElements());
        for (Blog blog : page.getContent()) {
            System.out.println(blog);
        }
    }
}
